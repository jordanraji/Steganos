import base64
import os
import sys, getopt
from PIL import Image, ImageMath
from Crypto.PublicKey import RSA
from Crypto.Cipher import AES
from Crypto import Random

# GLOBAL VARIABLES
SIZE_RSA_KEYS = 1024
rsa_keys = RSA.generate(SIZE_RSA_KEYS)
aes_key = b'Sixteen byte key'
BLOCK_SIZE_AES = 16
PADDING_AES = '{'
aes_cipher = AES.new(aes_key)

"""
METODOS RSA
=============================================
metodos propios de los conceptos de RSA
"""


def GenerateRSAKeys(private_key_, public_key_):
    """
        Método para que un usuario genere su llave pública y privada RSA
    """
    f = open(private_key_, 'w')
    f.write(rsa_keys.exportKey('PEM'))
    f.close()
    f = open(public_key_, 'w')
    f.write(rsa_keys.publickey().exportKey('PEM'))
    f.close()


def ReadPrivateKey(key):
    """
        Lee llave privada
    """
    f = open(key, 'r')
    global rsa_keys
    rsa_keys = RSA.importKey(f.read())


def ReadPublicKey(key):
    """
        Lee llave publica
    """
    f = open(key, 'r')
    global rsa_keys
    rsa_keys = RSA.importKey(f.read())


def EncryptRSA(str_):
    """
        Cifra el mensaje usando una llave pública RSA
    """
    enc_data = rsa_keys.publickey().encrypt(str_, 32)
    return enc_data


def DecryptRSA(enc_data):
    """
        Descifra el mensaje oculto usando una llave privada RSA
    """
    dec_data = rsa_keys.decrypt(enc_data)
    return dec_data

"""
METODOS AES
=============================================
metodos propios de los conceptos de AES
"""


def GenerateAESKey():
    """
        Genera una llave AES
    """
    global aes_key
    aes_key = os.urandom(BLOCK_SIZE_AES)


def GenerateAESCipher():
    """
        Inicializa AES
    """
    global aes_cipher
    aes_cipher = AES.new(aes_key)


def pad(s):
    """
        Helper para usar AES
    """
    return s + (BLOCK_SIZE_AES - len(s) % BLOCK_SIZE_AES) * PADDING_AES

EncryptAES = lambda c, s: base64.b64encode(c.encrypt(pad(s))) # Funcion para cifrar mensaje usando AES
DecryptAES = lambda c, e: c.decrypt(base64.b64decode(e)).rstrip(PADDING_AES) # Funcion para descifrar usando AES

"""
METODOS ESTEGANOGRAFICOS - LSB
=============================================
Se usa el conceptos del bit menos significativos (LSB)
para el proceso esteganografico
"""


def LSB(list_, txt_):
    """
        Metodo del Bit menos significativo en imagenes
        Reemplaza el bit menos significativo de cada uno de los valores RGB de cada pixel
        con nuevos valores
    """
    size_list_ = len(list_)
    size_txt_ = len(txt_)
    i = 0
    while (i < size_list_):
        j = 0
        list_[i] = list(list_[i])
        while (j < 3):
            if ((3 * i + j) < size_txt_):
                # list_[i][j][7] = txt_[3*i+j]
                list_[i][j] = list_[i][j][:-1]
                list_[i][j] += txt_[3 * i + j]
            j = j + 1
        list_[i] = tuple(list_[i])
        i = i + 1
    return list_


def GetLBSFromList(list_):
    """
        Metodo inverso de LSB. Obtiene la informacion del bit menos significativo
        de los valores RGB 
    """
    final_ = ""
    for tuple_ in list_:
        for string_ in tuple_:
            final_ += string_[7]
    return final_

"""
METODOS COMPLEMENTARIOS HELPERS
=============================================
metodos basicos para su uso en otras funciones
"""


def ReplaceInStr(str_, letter_, idx_):
    s_ = list(str_)
    s_[idx_] = letter_
    out_ = "".join(s_)
    return out_


def File2Str(text_file_):
    with open(text_file_, 'r') as myfile:
        # text_clear_ = myfile.read().replace('\n', '')
        text_clear_ = myfile.read()
    return text_clear_


def Str2File(str_, output_name_):
    fo = open(output_name_, "wb")
    fo.write(str_)
    fo.close()


def Str2BinaryAsList(str_):
    bin_ = [bin(ord(ch))[2:].zfill(8) for ch in str_]
    return bin_


def BinaryTupleList2TupleList(binary_tuple_list_):
    temp_ = []
    for tuple_ in binary_tuple_list_:
        for string_ in tuple_:
            temp_.append(int(string_, 2))

    pre_tup_ = []
    final_ = []
    i = 0
    while(i < len(temp_)):
        if (i % 3 == 0):
            pre_tup_ = []
        pre_tup_.append(temp_[i])
        if (i % 3 == 2):
            final_.append(tuple(pre_tup_))
        i += 1

    return final_


def Str2Binary(str_):
    bin_ = ''
    t = Str2BinaryAsList(str_)
    for letter in t:
        bin_ += letter
    return bin_


def Binary2Str(str_):
    message = ""
    while str_ != "":
        i = chr(int(str_[:8], 2))
        message = message + i
        str_ = str_[8:]
    return message


def Image2BinaryTupleList(image_):
    im = Image.open(image_)
    binaries_list = []
    for tup in im.getdata():
        tup_t = ()
        for value in tup:
            tup_t = tup_t + (bin(value)[2:].zfill(8),)
        binaries_list.append(tup_t)
    return binaries_list


def Image2TupleList(image_):
    im = Image.open(image_)
    list_ = list(im.getdata())
    return list_


def GenerateImage(list_, name_, input_image_):
    im_ = Image.open(input_image_)
    output_ = Image.new(im_.mode, im_.size)
    output_.putdata(list_)
    output_.save(name_)


"""
METODOS DE CIFRADO - DESCIFRADO DE UN MENSAJE
=============================================
Metodos de cifrado sin utilizar esteganografia.
Utilizan el concepto de 'sobre digital'.
"""


def Encrypt(str_):
    GenerateAESKey()
    GenerateAESCipher()
    encoded_text_ = EncryptAES(aes_cipher, str_)
    ReadPublicKey('public_key.pem')
    encoded_key_ = EncryptRSA(aes_key)
    return encoded_key_[0] + encoded_text_


def Decrypt(enc_):
    encoded_key_ = enc_[:128]
    encoded_text_ = enc_[128:]
    ReadPrivateKey('private_key.pem')
    global aes_key
    aes_key = DecryptRSA(encoded_key_)
    GenerateAESCipher()
    decoded_text_ = DecryptAES(aes_cipher, encoded_text_)
    return decoded_text_


"""
METODOS DE ESTEGANOGRAFIA
=============================================
Metodos para utilizar esteganografia.
Utilizan el concepto de 'sobre digital' y LSB.
"""


def Encode(text_file_, image_, public_key_, output_name_):
    """
    Crea la imagen con la información oculta.
    """
    clear_text_ = File2Str(text_file_) #Obtiene el texto del archivo.
    GenerateAESKey()
    GenerateAESCipher()
    encoded_text_ = EncryptAES(aes_cipher, clear_text_) # Cifra el texto claro con llave AES
    ReadPublicKey(public_key_) # Lee la llave publica del receptor del mensaje
    encoded_key_ = EncryptRSA(aes_key)[0] # Cifra la llave AES con la llave del receptor
    binary_encoded_key_ = Str2Binary(encoded_key_) # Llave AES convertida a binario
    binary_encoded_text_ = Str2Binary(encoded_text_) # texto cifrado AES a binario
    binary_encoded_full_ = binary_encoded_key_ + binary_encoded_text_ # Concatenacion
    image_binary_list_ = Image2BinaryTupleList(image_) # Imagen convertida a lista de tuplas con informacion RGB en binario
    image_binary_encoded_list_ = LSB(image_binary_list_, binary_encoded_full_) # Metodo LBS para ocultar la informacion en la imagen
    image_encoded_list_ = BinaryTupleList2TupleList(image_binary_encoded_list_) # Convierte RGB binario a Decimal
    GenerateImage(image_encoded_list_, output_name_, image_) # Genera la imagen con informacion oculta
    # print aes_key


def Decode(image_, private_key_, output_name_):
    """
    Decodifica la imagen con la imformación oculta
    """
    binary_image_encoded_prev = Image2TupleList(image_) # Imagen a tuplas RGB
    binary_image_encoded_ = Image2BinaryTupleList(image_) # Tuplas RGB decimales a RGB binarias
    binary_encoded_full_ = GetLBSFromList(binary_image_encoded_) # Proceso inverso de LBS para obtener informacion oculta
    binary_encoded_key_ = binary_encoded_full_[:1024] # Obtiene la llave AES cifrada en binario
    binary_encoded_text_ = binary_encoded_full_[1024:] # Obtiene el texto cifrado con AES en binario
    encoded_key_ = Binary2Str(binary_encoded_key_) # Obtiene la llave AES cifrada
    encoded_text_ = Binary2Str(binary_encoded_text_) # Obtiene el texto cifrado con AES
    ReadPrivateKey(private_key_) # Lee la llave privada del receptor
    global aes_key 
    aes_key = DecryptRSA(encoded_key_) # Descifra la llave AES usando la llave privada RSA
    GenerateAESCipher() 
    decoded_text_ = DecryptAES(aes_cipher, encoded_text_) # Descifra el mensaje oculto utilizando la llave AES
    Str2File(decoded_text_, output_name_) # Genera archivo con el mensaje oculto
    # print aes_key


"""
MAIN
=============================================
funcion principal
"""


def main():
    print "   _____ __                                   "
    print "  / ___// /____  ____  ____  ____  ____  _____"
    print "  \__ \/ __/ _ \/ __ `/ __ `/ __ \/ __ \/ ___/"
    print " ___/ / /_/  __/ /_/ / /_/ / / / / /_/ (__  ) "
    print "/____/\__/\___/\__, /\__,_/_/ /_/\____/____/  "
    print "              /____/                          "
    print "                                  v.0.0.1-beta"
    # option = raw_input("enter (e/d): ")
    # if (option == "e"):
    #     file_input = raw_input("txt input: ")
    #     image_input = raw_input("image input: ")
    #     public_key = raw_input("receiver public key: ")
    #     image_encoded = "out.png"
    #     Encode(file_input, image_input, public_key, image_encoded)
    #
    # if (option == "d"):
    #     image_encoded = raw_input("image encoded: ")
    #     private_key = raw_input("your private key: ")
    #     file_output = "out.txt"
    #     Decode(image_encoded, private_key, file_output)
    #
    # if (option!="d" and option!="e"):
    #     print "error!"

    # GenerateRSAKeys("pr_bob.pem", "pu_bob.pem")

    Encode("text.txt", "city.jpg", "public_key.pem", "out.png")
    # Decode("out.png", "private_key.pem", "out.txt")

    print "Done!"
main()
