# Steganos

Programa escrito en Python para la esteganografía en imágenes.

Curso: Seguridad en Computación

Año: 2016-2

Alumnos:

1. Jordan Raji Lazo Cahua
2. Julio Jesús Ticona Quispe
3. Jesus Erick Vera Callme
4. André Lui Ramos Provincia


# Instalación

Instalar [Python 2.7](https://www.python.org/download/releases/2.7/) y PyPA tool para la instalación de paquetes Python [pip](https://pip.pypa.io/en/stable/installing/)

## Dependencias
Steganos tiene algunas dependencias, utiliza el siguiente comando para poder instalar las dependencias.

```
$ pip install -r requirements.txt
```

# Ejecución
```
$ python steganos.py
```